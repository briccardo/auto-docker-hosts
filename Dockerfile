# Use an official Python runtime as a parent image
FROM python:3.9-slim

# Set environment variables to ensure output is unbuffered and pip cache is disabled
ENV PYTHONUNBUFFERED=1 PIP_NO_CACHE_DIR=1

# Copy only the requirements.txt file first, to leverage Docker layer caching
COPY requirements.txt /app/

# Install any needed packages specified in requirements.txt
RUN pip install --upgrade pip && \
    pip install -r /app/requirements.txt

# Set the working directory
WORKDIR /app

# Copy the rest of the application code
COPY . /app

# Run the application
CMD ["python", "app.py"]