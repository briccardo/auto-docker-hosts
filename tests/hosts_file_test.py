import os
from typing import List

from app import AutoDockerHosts, SimplifiedContainer
import pytest
from shutil import copyfile
from pathlib import Path

from tests import get_path


@pytest.fixture(autouse=True)
def make_temp_file():
    copyfile(get_path('data/default_hosts'), '/tmp/default_hosts')
    copyfile(get_path('data/some_automatic_hosts'), '/tmp/some_automatic_hosts')


def test_delete_all_entries_in_hosts_file():
    AutoDockerHosts(run=False, hosts_file_path='/tmp/some_automatic_hosts').delete_all_entries_in_hosts_file()

    default_hosts_content = Path('/tmp/default_hosts').read_text()
    some_automatic_hosts_content = Path('/tmp/some_automatic_hosts').read_text()

    assert default_hosts_content == some_automatic_hosts_content


def test_using_default_formatting(mocker):
    test_file = Path('/tmp/default_formatting_test_hosts')

    # Create the file
    test_file.touch()

    def get_simplified_container_list_mock(self) -> List[SimplifiedContainer]:
        return [SimplifiedContainer('1', 'container_name', 'container_address')]

    mocker.patch('app.AutoDockerHosts.get_simplified_container_list', get_simplified_container_list_mock)
    AutoDockerHosts(run=False, hosts_file_path='/tmp/default_formatting_test_hosts').update_hosts()

    # Assert
    assert test_file.read_text() == 'container_address container_name.local # auto-docker-hosts' + os.linesep


def test_using_custom_formatting(mocker, monkeypatch):
    test_file = Path('/tmp/custom_formatting_test_hosts')

    # Create the file
    test_file.touch()

    monkeypatch.setenv('HOSTS_ENTRY_FORMAT', '{address} {domain}')

    def get_simplified_container_list_mock(self) -> List[SimplifiedContainer]:
        return [SimplifiedContainer('1', 'container_name', 'container_address')]

    mocker.patch('app.AutoDockerHosts.get_simplified_container_list', get_simplified_container_list_mock)
    AutoDockerHosts(run=False, hosts_file_path='/tmp/custom_formatting_test_hosts').update_hosts()

    # Assert
    assert test_file.read_text() == 'container_address container_name # auto-docker-hosts' + os.linesep


def test_get_host_entry_address_is_invalid():
    test_file = Path('/tmp/get_host_entry_test_hosts')
    test_file.touch()
    auto_docker_hosts = AutoDockerHosts(run=False, hosts_file_path=str(test_file))

    container_with_invalid_address: SimplifiedContainer = SimplifiedContainer('1', 'container_name', '')
    assert auto_docker_hosts.get_host_entry_from_simplified_container(container_with_invalid_address) is None

    container_with_invalid_address: SimplifiedContainer = SimplifiedContainer('1', 'container_name', None)
    assert auto_docker_hosts.get_host_entry_from_simplified_container(container_with_invalid_address) is None

    container_with_invalid_address: SimplifiedContainer = SimplifiedContainer('1', 'container_name', 'asd')
    assert auto_docker_hosts.get_host_entry_from_simplified_container(container_with_invalid_address) is not None


