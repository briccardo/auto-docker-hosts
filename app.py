import datetime
import logging
import os
import sys
from dataclasses import dataclass
from typing import Optional, List

import docker
from apscheduler.schedulers.background import BackgroundScheduler
from docker.models.containers import Container


def setup_logging() -> None:
    """
    Sets up logging to a file and console.

    This function configures the root logger to log messages at the INFO level to a file and console. T
    he log file is named 'auto-docker-hosts.log' and is located in the same directory as the script that calls this function.
    The log messages are formatted with a timestamp, log level, and message, and are written in UTF-8 encoding.

    Returns:
        None.
    """
    log = logging.getLogger()
    log.setLevel(logging.INFO)
    log_format = logging.Formatter("%(asctime)s %(levelname)s - %(message)s", '%Y-%m-%d %H:%M:%S')

    log_path = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'auto-docker-hosts.log')
    file_handler = logging.FileHandler(filename=log_path, encoding='utf-8', mode='w')
    file_handler.setFormatter(log_format)
    log.addHandler(file_handler)

    stdout_handler = logging.StreamHandler(sys.stdout)
    stdout_handler.setFormatter(log_format)
    log.addHandler(stdout_handler)


@dataclass
class SimplifiedContainer:
    """
    A simplified representation of a Docker container.

    This class contains the container's ID, name, and IP address as strings. It is used to create a subset of the
    container's properties that can be used to generate a host entry for the container.

    Attributes:
        id: A string representing the container's ID.
        name: A string representing the container's name.
        address: A string representing the container's IP address.
    """
    id: str
    name: str
    address: str


class AutoDockerHosts:
    def __init__(self, run: bool = True, hosts_file_path: str = '/parent-etc/hosts') -> None:
        """
        Initializes an AutoDockerHosts instance.

        This method sets up an instance of AutoDockerHosts with the given parameters.
        The `run` parameter determines whether to start the background scheduler and listen to Docker events
        immediately after initialization.

        The `hosts_file_path` parameter specifies the path to the hosts file to update.

        If the hosts file does not exist at the specified path, the method exits with an error message.

        The custom hosts entry format can be specified by setting the `HOSTS_ENTRY_FORMAT` environment variable.
        The default format is '{address} {domain}.local'.

        The last updated list is initially empty, and the logger is set to the root logger.

        If `run` is True, the Docker client is created and the background scheduler is started.
        The hosts file is cleared of all entries, and the method begins listening to Docker events.

        Args:
            run: A boolean indicating whether to start the background scheduler and listen to Docker events.
            hosts_file_path: A string representing the path to the hosts file to update.

        Returns:
            None.
        """
        self.hosts_file_path = hosts_file_path

        # Read the custom hosts entry format variable
        self.hosts_entry_format = os.getenv('HOSTS_ENTRY_FORMAT', '{address} {domain}.local')

        if not os.path.exists(self.hosts_file_path):
            exit(f'Hosts file at {self.hosts_file_path} does not seem to exist. Exiting.')

        self.last_updated_list: List[SimplifiedContainer] = []
        self.log = logging.getLogger()

        if run:
            self.client = docker.from_env()

            self.scheduler = BackgroundScheduler()

            self.log.info('Clearing host list...')
            self.delete_all_entries_in_hosts_file()

            self.start_scheduler()
            self.listen_to_docker_events()

    def start_scheduler(self) -> None:
        """
        Starts the background scheduler.

        This method adds a job to the background scheduler that runs the `update_hosts()` method every 60 seconds.
        The next run time is set to the current time. The scheduler is then started.

        Args:
            None.

        Returns:
            None.
        """
        self.scheduler.add_job(self.update_hosts, 'interval', seconds=60, next_run_time=datetime.datetime.now())
        self.scheduler.start()

    def listen_to_docker_events(self) -> None:
        """
        Listens to Docker events and updates the hosts file.

        This method listens to Docker events using the Docker client and filters for container-related
        events with actions of 'start', 'stop', or 'kill'.

        When a matching event is detected, the method logs a message indicating that the hosts file is being updated,
        and then calls the `update_hosts()` method to update the file.

        Args:
            None.

        Returns:
            None.
        """
        valid_actions = {'start', 'stop', 'kill'}

        for event in self.client.events(decode=True):
            if event.get('Type') != 'container':
                continue

            event_action = event.get('Action')
            if event_action not in valid_actions:
                continue

            self.log.info(f'Updating hosts because {event_action} event fired...')
            self.update_hosts()

    def update_hosts(self) -> None:
        """
        Update the hosts file with entries for each container in the simplified container list.

        This method retrieves the simplified container list using `get_simplified_container_list()`.
        It compares the list with the last updated list to determine if changes have been made.
        If the two lists are identical, the update is skipped and a message is logged.
        Otherwise, the method updates the last updated list and proceeds to update the hosts file.

        It deletes all entries and appends a new entry for each container in the simplified container list
        that has a valid host entry using `get_host_entry_from_simplified_container()` method.
        If there's no valid host entry, the method skips that container and moves to the next one.

        Returns:
            None
        """
        simplified_containers = self.get_simplified_container_list()
        if len(self.last_updated_list) == len(simplified_containers) and self.last_updated_list == simplified_containers:
            self.log.info('Skipping scheduled update since list has not changed...')
            return
        self.last_updated_list = simplified_containers

        self.log.info('Updating hosts...')
        self.delete_all_entries_in_hosts_file()
        for simplified_container in simplified_containers:
            host_entry = self.get_host_entry_from_simplified_container(simplified_container)
            if host_entry is None:
                continue
            self.append_line_to_hosts_file(host_entry)

    def get_simplified_container_list(self) -> List[SimplifiedContainer]:
        """
        Get a list of simplified containers from the Docker client's container list.

        This method retrieves a list of containers using the `containers.list()` method.
        It then maps each container to a `SimplifiedContainer` object using the `map_container_to_simplified_container()` method.
        Each `SimplifiedContainer` object contains a subset of the container's properties.
        The method appends each `SimplifiedContainer` object to a list and returns the list.

        Returns:
            A list of `SimplifiedContainer` objects.
        """
        return [self.map_container_to_simplified_container(container) for container in self.client.containers.list()]

    def get_flat_container_id_list(self) -> List[str]:
        """
        Get a list of container IDs as strings from the Docker client's container list.

        This method retrieves a list of containers using the `containers.list()` method.
        It maps each container to its ID string using a lambda function and the `map()` method.
        The resulting list of ID strings is returned.

        Returns:
            A list of container IDs as strings.
        """
        return [container.id for container in self.client.containers.list()]

    def get_container_address(self, container_id: str) -> Optional[str]:
        """
        Returns the network address for the specified container. If the container has multiple network addresses,
        selects the one with the lowest index. Returns None if no address is found.

        Args:
            container_id (str): The ID of the container.

        Returns:
            Optional[str]: The network address of the container, or None if not found.
        """
        try:
            container: Container = self.client.containers.get(container_id)
        except Exception as e:
            self.log.error(f'Could not inspect container with id {container_id}! Error: {e}')
            return None

        network_settings = container.attrs.get('NetworkSettings')
        if network_settings is None:
            return None

        networks = network_settings.get('Networks', {})
        sorted_networks = sorted(networks.items(), key=lambda x: x[0])
        for key, network in sorted_networks:
            return network.get('IPAddress')

        return None

    def map_container_to_simplified_container(self, source: Container) -> SimplifiedContainer:
        """
        Map a Docker container object to a SimplifiedContainer object.

        This method takes a `Container` object as input and uses its properties to create a
        `SimplifiedContainer` object. The `source.id` and `source.name` properties are directly
        assigned to the `SimplifiedContainer` object's ID and name properties, respectively.

        The method then calls `get_container_address()` to retrieve the container's IP address
        and assign it to the `SimplifiedContainer` object's address property. If `get_container_address()`
        raises an exception, the method logs an error and sets the address to None. If `get_container_address()`
        returns None, the method logs a warning but continues with the `SimplifiedContainer` object creation.

        The resulting `SimplifiedContainer` object is returned.

        Args:
            source: A `Container` object.

        Returns:
            A `SimplifiedContainer` object.
        """
        try:
            container_address = self.get_container_address(source.id)
        except Exception as e:
            self.log.error(f'Could not inspect container with id {source.id}! Error: {e}')
            container_address = None
        else:
            if container_address is None:
                self.log.warning(f'No network address found for container with id {source.id}!')

        return SimplifiedContainer(source.id, source.name, container_address)

    def append_line_to_hosts_file(self, line: str) -> None:
        """
        Append a line to the hosts file if it does not already exist.

        This method takes a string `line` as input and checks if it already exists in the hosts file.
        If it does, the method returns without making any changes.
        Otherwise, the method opens the hosts file using the `open()` function with mode 'a' to append the line.
        The method then writes the line to the file with a newline character using the `write()` method.

        If the method is unable to write to the file due to a `PermissionError`, it logs a critical error message.

        Args:
            line: A string representing the line to append to the hosts file.

        Returns:
            None
        """
        try:
            with open(self.hosts_file_path, 'a') as hosts_file:
                hosts_file.write(f'{line}{os.linesep}')
        except FileNotFoundError:
            self.log.critical(f'Could not find {self.hosts_file_path}, please check if the file exists.')
        except PermissionError:
            self.log.critical(f'Could not write to {self.hosts_file_path}, no permissions.')

    def delete_all_entries_in_hosts_file(self) -> None:
        """
        Delete all entries in the hosts file that were created by this program.

        This method first reads all the lines in the hosts file using `readlines()`.
        It then opens the hosts file again in write mode and loops through each line in the file.
        If the line contains the string '# auto-docker-hosts', it is left untouched.
        Otherwise, the line is deleted from the file by not writing it to the file.

        Returns:
            None
        """
        with open(self.hosts_file_path, 'r+') as hosts_file:
            lines = hosts_file.readlines()
            hosts_file.seek(0)
            hosts_file.truncate()
            for line in lines:
                if '# auto-docker-hosts' in line:
                    hosts_file.write(line)

    def check_line_exists_in_hosts_file(self, line: str) -> bool:
        """
        Check if a line exists in the hosts file.

        This method takes a string `line` as input and checks if it already exists in the hosts file.
        It opens the hosts file in read mode and loops through each line in the file.
        If the line stripped of whitespace matches `line`, the method returns True.
        If the file cannot be opened, the method logs an error and returns None.

        Args:
            line: A string representing the line to search for in the hosts file.

        Returns:
            A boolean indicating whether the line exists in the hosts file.
        """
        try:
            with open(self.hosts_file_path, 'r') as hosts_file:
                for current_line in hosts_file:
                    if current_line.rstrip() == line:
                        return True
        except IOError as e:
            self.log.error(f'Error reading hosts file: {self.hosts_file_path}. Error: {e}')

        return False

    def get_host_entry_from_simplified_container(self, source: SimplifiedContainer) -> Optional[str]:
        """
        Get a host entry string from a SimplifiedContainer object.

        This method takes a `SimplifiedContainer` object as input and creates a host entry string using the object's
        `address` and `name` properties. If the container's address or name are missing or empty, the method returns None.

        The method uses the `hosts_entry_format` string property to format the host entry string.
        The formatted string is then returned with a comment appended to the end indicating that the entry was
        automatically added by this program.

        Args:
            source: A `SimplifiedContainer` object.

        Returns:
            A string representing the host entry for the container, or None if the container address or name are missing.
        """
        if source.address is None:
            return None

        if len(source.address) <= 0:
            return None

        entry = self.hosts_entry_format.format(address=source.address, domain=source.name)
        return '{entry} # auto-docker-hosts'.format(entry=entry)


if __name__ == '__main__':
    setup_logging()
    AutoDockerHosts()
