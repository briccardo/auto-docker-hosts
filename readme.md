# auto-docker-hosts

🪄Automagically generates .local host entries for every running docker container.

Examples:

- A container named `simple-file-browser_app_1` running on port `5500` can be expected at `http://simple-file-browser_app_1.local:5500`

# When does it update the host file

- Every minute
- On every `start`, `stop`, `kill` docker event

# Run it

```yml
version: '3'

services:
  app:
    image: registry.gitlab.com/briccardo/auto-docker-hosts:latest
    restart: always
    volumes:
      - /var/run/docker.sock:/tmp/docker.sock
      - /etc:/parent-etc/
    environment:
      - DOCKER_HOST=unix://tmp/docker.sock
      - HOSTS_ENTRY_FORMAT={address} {domain}.local
```

`docker-compose up -d`

Or directly:

```shell
docker run --name auto-docker-hosts -e DOCKER_HOST='unix://tmp/docker.sock' -e HOSTS_ENTRY_FORMAT='{address} {domain}.local' -v /var/run/docker.sock:/tmp/docker.sock -v /etc:/parent-etc/ --restart always -d registry.gitlab.com/briccardo/auto-docker-hosts:latest
```

# Test it

`docker run --name nginx -d nginx:1.17.2-alpine`

Now browse to `nginx.local` and... 🎩

# Configuration

The environment variable `HOSTS_ENTRY_FORMAT` can be used to modify how entries are generated.

The default value is `{address} {domain}.local` which will result in `/etc/hosts` entries like: `172.24.0.2 comicdl.local # auto-docker-hosts`

The `{address}` variable is the container local IP, and `{domain}` is the docker container name.

At the end of every entry automatically generated, the string `# auto-docker-hosts` will be appended, and this cannot be disabled.

The reason is that the program uses said string to match which lines were automatically generated versus those that aren't.