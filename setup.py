from setuptools import setup
from version import __version__

setup(
    name='auto-docker-hosts',
    description='Automagically generates .local host entries for every docker container.',
    version=__version__,
    python_requires='>=3.8',
)

